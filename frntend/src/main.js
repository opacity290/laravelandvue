import Vue from 'vue'
import vueResource from 'vue-resource'
import App from './App'
import Router from 'vue-router'

Vue.config.productionTip = false
Vue.use(Router)

Vue.use(vueResource)
const routes = [
  {path: '/', name: 'home', component: App}
]

const router = new Router({
  mode: 'history',
  routes
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
