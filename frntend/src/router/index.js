import Vue from 'vue'
import Router from 'vue-router'
import App from '@/App'

Vue.use(Router)

const routes = [
  {path: '/', name: 'home', component: App}
]
export default new Router({
  mode: 'history',
  routes
})
